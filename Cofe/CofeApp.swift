//
//  CofeApp.swift
//  Cofe
//
//  Created by danyaq on 08.09.2021.
//

import SwiftUI

@main
struct CofeApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
