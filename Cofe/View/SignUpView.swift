//
//  SignUpView.swift
//  Cofe
//
//  Created by danyaq on 08.09.2021.
//

import SwiftUI

struct SignUpView: View {
    @Binding var selectedPage: Int
    @State var email = ""
    var body: some View {
        ZStack{
            Image("Group 1")
                .frame(maxHeight: .infinity, alignment: .top)
                .frame(maxWidth: UIScreen.main.bounds.width)
                .offset(y: -50)
            VStack{
                Spacer()
                VStack(spacing: 10  ){
                Text("What’s your email address?")
                    .lineLimit(2)
                    .font(.system(size: 40, weight: .bold))
                    .foregroundColor(Color.init("z"))
                    .padding(.leading, 26)
                    .padding(.trailing, 40)
                    VStack(alignment: .leading,spacing: 20){
                        Text("YOUR EMAIL")
                            .font(.system(size: 13, weight: .bold))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.7411764706, green: 0.7254901961, blue: 0.7137254902, alpha: 1)))
                            .frame(maxWidth: .infinity, alignment: .leading)
                        VStack{
                            HStack{
                                ZStack{
                                    if email.isEmpty{
                                        Text("marcosardido@gmail.com")
                                            .foregroundColor(Color.black)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    }
                            TextField("", text: $email)
                                }
                                .font(.system(size: 18, weight: .medium))
                                Image("x-circle")
                                    .onTapGesture {
                                        email = ""
                                    }
                            }
                            Rectangle()
                                .frame(height: 1)
                                .foregroundColor(Color.init("z"))
                        }
                        Button(action: {
                            selectedPage = 3
                        }, label: {
                            HStack(spacing: 6){
                                Image("mail")
                                Text("Continue with Email")
                                    .font(.system(size: 16, weight: .bold))
                                   
                            }
                            .frame(maxWidth: .infinity, alignment: .center)
                            .frame(height: 55)
                            .background(Color.init("z"))
                            .cornerRadius(30)
                          
                            
                        })
                        .foregroundColor(.white)
                        .padding(.top)
                    }
                    .padding(.horizontal, 48)
                    .padding(.bottom, 40)
                    
                }
                
            }
        }
        .edgesIgnoringSafeArea(.top)
    }
}

