//
//  DetailView.swift
//  Cofe
//
//  Created by Daniil on 08.09.2021.
//

import SwiftUI

struct DetailView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var size = 0
    var body: some View {
        ZStack{
            Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)).edgesIgnoringSafeArea(.top)
            VStack{
                Image("bb")
                Spacer()
                
            }
            VStack(spacing: 0){
                Spacer()
                HStack(spacing: 0){
                    Button(action: {}, label: {
                        Image("call")
                            .frame(width: 56, height: 56)
                            .background(Color.white)
                            .cornerRadius(8)
                            .overlay(RoundedRectangle(cornerRadius: 8).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.15))))
                    })
                    Spacer(minLength: 16)
                    Button(action: {}, label: {
                        Text("Buy Now")
                            .font(.system(size: 18, weight: .semibold))
                            .foregroundColor(Color.white)
                            .frame(maxWidth: .infinity)
                            .frame(height: 56)
                            .background(Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)))
                            .cornerRadius(12)
                            
                    })
                }
                .padding(.horizontal, 20)
                .padding(.top, 20)
                .padding(.bottom, 10)
                .background(Color.white)
                .clipShape(CustomShape(width: 12, height: 12))
                .shadow(color: Color.black.opacity(0.10), radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
                
            }
            .zIndex(1)
            VStack(spacing: 0){
              
                VStack(spacing: 0){
                    HStack{
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Image("le")
                                .frame(width: 34, height: 34)
                                .background(Color.white)
                                .cornerRadius(8)
                        })
                        Spacer()
                        Button(action: {
                            
                        }, label: {
                            Image("he")
                                .frame(width: 34, height: 34)
                                .background(Color.white)
                                .cornerRadius(8)
                        })
                    }
                    Image("cc")
                        .resizable()
                        .frame(width: 145.88, height: 166)
                        .padding(.top, 43)
                }
                .padding(.top, 48)
                .padding(.bottom, 50)
                .padding(.horizontal)
               
                ScrollView(.vertical, showsIndicators: false, content: {
                    VStack(spacing: 0){
                        HStack{
                            VStack(spacing: 8){
                                Text("$ 4.20")
                                    .font(.system(size: 24, weight: .bold))
                                    .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 1)))
                                HStack(spacing: 5.5){
                                    Image("star")
                                    Text("4.2 (555)")
                                        .font(.system(size: 14))
                                        .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 1)))
                                }
                            }
                            Spacer()
                            HStack(spacing: 12){
                                VStack(spacing:9){
                                    Image("j")
                                    Text("Coffee")
                                        .font(.system(size: 12, weight: .medium))
                                        .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.6)))
                                }
                                .frame(width: 58, height: 65)
                                .background(Color.init(#colorLiteral(red: 0.9803921569, green: 0.9490196078, blue: 0.9215686275, alpha: 1)))
                                .cornerRadius(12)
                                VStack(spacing:9){
                                    Image("jj")
                                    Text("Milk")
                                        .font(.system(size: 12, weight: .medium))
                                        .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.6)))
                                }
                                .frame(width: 58, height: 65)
                                .background(Color.init(#colorLiteral(red: 0.9803921569, green: 0.9490196078, blue: 0.9215686275, alpha: 1)))
                                .cornerRadius(12)
                            }
                        }
                        .padding(.bottom,25)
                        VStack(alignment: .leading, spacing: 8){
                            Text("Cappuccino")
                                .font(.system(size: 27, weight: .bold))
                                .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 1)))
                                .frame(maxWidth: .infinity, alignment: .leading)
                         //fix
                            Text("A cappuccino is an espresso-based coffee drink that originated in Italy, and is traditionally...[More]")
                                .font(.system(size: 14))
                                .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.9)))
                        }
                        VStack(alignment: .leading, spacing: 12){
                            Text("Size")
                                .font(.system(size: 16, weight: .medium))
                                .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 1)))
                                .frame(maxWidth: .infinity, alignment: .leading)
                            HStack(spacing:17){
                                Button(action: {
                                    size = 1
                                }, label: {
                                    Text("S")
                                        .font(.system(size: 20, weight: .semibold))
                                        .foregroundColor(size == 1 ? Color.init(#colorLiteral(red: 0.9568627451, green: 0.5215686275, blue: 0.2588235294, alpha: 0.9)) : Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.9)))
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .frame(height: 44)
                                        .background(size == 1 ? Color.init(#colorLiteral(red: 0.9803921569, green: 0.9490196078, blue: 0.9215686275, alpha: 0.15)) : Color.white)
                                        .cornerRadius(8)
                                        .overlay(RoundedRectangle(cornerRadius: 8).stroke(lineWidth: 1).foregroundColor(size == 1 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 0.5)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.15))))
                                })
                                .overlay(Image("ch").offset(y: -15).opacity(size == 1 ? 1 : 0), alignment: .top)
                                Button(action: {
                                    size = 2
                                }, label: {
                                    Text("M")
                                        .font(.system(size: 20, weight: .semibold))
                                        .foregroundColor(size == 2 ? Color.init(#colorLiteral(red: 0.9568627451, green: 0.5215686275, blue: 0.2588235294, alpha: 0.9)) : Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.9)))
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .frame(height: 44)
                                        .background(size == 2 ? Color.init(#colorLiteral(red: 0.9803921569, green: 0.9490196078, blue: 0.9215686275, alpha: 0.15)) : Color.white)
                                        .cornerRadius(8)
                                        .overlay(RoundedRectangle(cornerRadius: 8).stroke(lineWidth: 1).foregroundColor(size == 2 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 0.5)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.15))))
                                })
                                .overlay(Image("ch").offset(y: -15).opacity(size == 2 ? 1 : 0), alignment: .top)
                                Button(action: {
                                    size = 3
                                }, label: {
                                    Text("L")
                                        .font(.system(size: 20, weight: .semibold))
                                        .foregroundColor(size == 3 ? Color.init(#colorLiteral(red: 0.9568627451, green: 0.5215686275, blue: 0.2588235294, alpha: 0.9)) : Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 0.9)))
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .frame(height: 44)
                                        .background(size == 3 ? Color.init(#colorLiteral(red: 0.9803921569, green: 0.9490196078, blue: 0.9215686275, alpha: 0.15)) : Color.white)
                                        .cornerRadius(8)
                                        .overlay(RoundedRectangle(cornerRadius: 8).stroke(lineWidth: 1).foregroundColor(size == 3 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 0.5)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.15))))
                                })
                                .overlay(Image("ch").offset(y: -15).opacity(size == 3 ? 1 : 0), alignment: .top)
                            }
                        }
                        .padding(.top, 24)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 24)
                    .padding(.bottom, 120)
                })
                .background(Color.white)
                .clipShape(CustomShape(width: 24, height: 24))
            }
        }
        .onDisappear{
            size = 0
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.top)
       
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}
