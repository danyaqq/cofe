//
//  MainTabView.swift
//  Cofe
//
//  Created by Daniil on 08.09.2021.
//

import SwiftUI

struct MainTabView: View {
    @State var selection = 1
    init(){
        UITabBar.appearance().isHidden = true
    }
    var body: some View {
        NavigationView{
        VStack(spacing : 0){
            TabView(selection: $selection){
                HomeView()
                    .tag(1)
                Text("Favorite")
                    .tag(2)
                Text("Cart")
                    .tag(3)
                Text("Profile")
                    .tag(4)
            }
            .tabViewStyle(DefaultTabViewStyle())
            CustomTabView(selection: $selection)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        }
    }
}

struct CustomTabView: View{
    @Binding var selection: Int
    var body: some View{
        HStack{
            Button(action: {
                selection = 1
            }, label: {
                VStack(spacing: 4){
                    Image("1")
                        .renderingMode(.template)
                        .foregroundColor(selection == 1 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.8)))
                    Text("Home")
                        .foregroundColor(selection == 1 ? Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
                
            })
            Button(action: {
                selection = 2
            }, label: {
                VStack(spacing: 4){
                    Image("2")
                        .renderingMode(.template)
                        .foregroundColor(selection == 2 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.8)))
                    Text("Favourite")
                        .foregroundColor(selection == 2 ? Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
                
            })
            Button(action: {
                selection = 3
            }, label: {
                VStack(spacing: 4){
                    Image("3")
                        .renderingMode(.template)
                        .foregroundColor(selection == 3 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.8)))
                    Text("Cart")
                        .foregroundColor(selection == 3 ? Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
                
            })
            Button(action: {
                selection = 4
            }, label: {
                VStack(spacing: 4){
                    Image("4")
                        .renderingMode(.template)
                        .foregroundColor(selection == 4 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1176470588, green: 0.06666666667, blue: 0.0431372549, alpha: 0.8)))
                    Text("Profile")
                        .foregroundColor(selection == 4 ? Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
                
            })
        }
        .padding(.top, 12)
        .padding(.bottom, 4)
        .background(Color.white.edgesIgnoringSafeArea(.bottom))
        .clipShape(CustomShape(width: 12, height: 12))
        .shadow(color: Color.black.opacity(0.10), radius: 10, x: 0, y: 0)
        .mask(Rectangle().padding(.top, -20))
        
    }
}

struct CustomShape: Shape{
    let width: Int
    let height: Int
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight], cornerRadii: .init(width: width, height: height))
        return Path(path.cgPath)
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
