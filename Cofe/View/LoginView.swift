//
//  LoginView.swift
//  Cofe
//
//  Created by danyaq on 08.09.2021.
//

import SwiftUI

struct LoginView: View {
    @Binding var selectedPage: Int
    @State var password = ""
    @State var showPassword = false
    var body: some View {
        ZStack{
            Image("Group 2")
                .frame(maxHeight: .infinity, alignment: .top)
                .frame(maxWidth: UIScreen.main.bounds.width)
                .offset(y: -50)
            VStack{
                Spacer()
                VStack(alignment: .leading, spacing: 10  ){
                Text("Login")
                    
                    .lineLimit(2)
                    .font(.system(size: 40, weight: .bold))
                    .foregroundColor(Color.init("z"))
                  
                    
                    HStack{
                        Text("Using ")
                            .font(.system(size: 15, weight: .bold))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.7411764706, green: 0.7254901961, blue: 0.7137254902, alpha: 1)))
                        Text("marcosardido@gmail.com")
                            .font(.system(size: 15, weight: .bold))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.8235294118, green: 0.7058823529, blue: 0.6235294118, alpha: 1)))
                        Text(" to login")
                            .font(.system(size: 15, weight: .bold))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.7411764706, green: 0.7254901961, blue: 0.7137254902, alpha: 1)))
                    }
                    .padding(.bottom,15)
                    VStack(alignment: .leading,spacing: 20){
                        Text("YOUR PASWWORD")
                            .font(.system(size: 13, weight: .bold))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.7411764706, green: 0.7254901961, blue: 0.7137254902, alpha: 1)))
                            
                        VStack{
                            HStack{
                                ZStack{
                                    if password.isEmpty{
                                        Text("123123")
                                            .foregroundColor(Color.black)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    }
                                    if showPassword{
                                        TextField("", text: $password)
                                    } else {
                                        SecureField("", text: $password)
                                    }
                            
                                }
                                .font(.system(size: 18, weight: .medium))
                                Image("eye-off")
                                    .onTapGesture {
                                        withAnimation(.spring()){
                                            showPassword.toggle()
                                        }
                                    }
                            }
                            Rectangle()
                                .frame(height: 1)
                                .foregroundColor(Color.init("z"))
                        }
                        Button(action: {
                            selectedPage = 4
                        }, label: {
                            HStack(spacing: 6){
                            
                                Text("Sign In")
                                    .font(.system(size: 16, weight: .bold))
                                   
                            }
                            .frame(maxWidth: .infinity, alignment: .center)
                            .frame(height: 55)
                            .background(Color.init("z"))
                            .cornerRadius(30)
                          
                            
                        })
                        .foregroundColor(.white)
                        .padding(.top)
                    }
                   
                }
                .padding(.horizontal, 35)
                .padding(.bottom, 40)
            }
        }
        .edgesIgnoringSafeArea(.top)
    }
}


