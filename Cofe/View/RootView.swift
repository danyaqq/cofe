//
//  RootView.swift
//  Cofe
//
//  Created by danyaq on 08.09.2021.
//

import SwiftUI

struct RootView: View {
    @State var selectedPage = 1
    var body: some View {
        if selectedPage == 1{
            OnboardingView(selectedPage: $selectedPage)
        } else if selectedPage == 2{
            SignUpView(selectedPage: $selectedPage)
        }else if selectedPage == 3{
            LoginView(selectedPage: $selectedPage)
        } else if selectedPage == 4{
            MainTabView()
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
