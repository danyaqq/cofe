//
//  OnboardingView.swift
//  Cofe
//
//  Created by danyaq on 08.09.2021.
//

import SwiftUI

struct OnboardingView: View {
    @Binding var selectedPage: Int
    var body: some View {
        ZStack{
            Image("OnboardingPic1")
                .frame(maxHeight: .infinity, alignment: .top)
                .frame(maxWidth: UIScreen.main.bounds.width)
            VStack(spacing: 15){
            Spacer()
            HStack{
            Image("Title")
                Spacer()
            }
            .padding(.leading, 24)
                VStack(spacing: 16){
            Button(action: {
                selectedPage = 2
            }, label: {
                HStack(spacing: 6){
                    Image("mail")
                    Text("Continue with Email")
                        .font(.system(size: 16, weight: .bold))
                       
                }
                .frame(maxWidth: .infinity, alignment: .center)
                .frame(height: 55)
                .background(Color.init("z"))
                .cornerRadius(30)
              
                
            })
            .padding(.horizontal, 48)
            .foregroundColor(Color.white)
                HStack(spacing: 40){
                    Button(action: {
                        
                    }, label: {
                        Spacer()
                        Image("google")
                            Spacer()
                    })
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .frame(height: 55)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.7450980392, green: 0.568627451, blue: 0.4509803922, alpha: 1)).opacity(0.62)))
                    Button(action: {
                        
                    }, label: {
                        Spacer()
                        Image("FB_Logo")
                            Spacer()
                    })
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .frame(height: 55)
                    .overlay(RoundedRectangle(cornerRadius: 30).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.7450980392, green: 0.568627451, blue: 0.4509803922, alpha: 1)).opacity(0.62)))
                }
                .padding(.horizontal, 35)
            }
          
        }
            .padding(.bottom, 30)
        }
        .edgesIgnoringSafeArea(.top)
    }
}
