//
//  HomeView.swift
//  Cofe
//
//  Created by Daniil on 08.09.2021.
//

import SwiftUI

struct HomeView: View {
    @State var search = ""
    @State var selected = 1
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false, content: {
        VStack(spacing: 0){
            ZStack{
            Image("b")
                .resizable()
                .scaledToFill()
                .frame(width: UIScreen.main.bounds.width)
                VStack(spacing: 23){
                    HStack{
                        Button(action: {
                            
                        }, label: {
                            Image("qq")
                                .frame(width: 34, height: 34)
                                .background(Color.white)
                                .cornerRadius(8)
                        })
                        Spacer()
                        Image("some")
                            .frame(width: 34, height: 34)
                            .cornerRadius(8)
                    }
                    Text("Find the best\ncoffee for you")
                        .font(.system(size: 28, weight: .black))
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .lineLimit(2)
                    HStack{
                        HStack(spacing: 6){
                            Image("search")
                            ZStack{
                                if search.isEmpty{
                                    Text("Try “Latte”")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                }
                                TextField("", text: $search)
                            }
                            .font(.system(size: 14))
                            .foregroundColor(Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)))
                        }
                        .padding(.horizontal, 16)
                        .padding(.vertical, 12)
                        .background(Color.white)
                        .cornerRadius(8)
                        Spacer(minLength: 12)
                        Button(action: {
                            
                        }, label: {
                            Image("map")
                                .frame(width: 44, height: 44)
                                .background(Color.white)
                                .cornerRadius(8)
                        })
                    }
                }
                .padding(.top, 42)
                .padding(.bottom, 20)
                .padding(.horizontal)
            }

            
            ScrollView(.horizontal, showsIndicators: false, content: {
                HStack(spacing: 23){
                    VStack(spacing: 10){
                        Text("Cappuccino")
                            .foregroundColor(selected == 1 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                        Circle()
                            .frame(width: 8, height: 8)
                            .foregroundColor(selected == 1 ? Color.orange : Color.clear)
                    }
                    .onTapGesture {
                        withAnimation(.spring()){
                            selected = 1
                        }
                    }
                    VStack(spacing: 10){
                        Text("Espresso")
                            .foregroundColor(selected == 2 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                        Circle()
                            .frame(width: 8, height: 8)
                            .foregroundColor(selected == 2 ? Color.orange : Color.clear)
                    }
                    .onTapGesture {
                        withAnimation(.spring()){
                            selected = 2
                        }
                    }
                    VStack(spacing: 10){
                        Text("Arabica")
                            .foregroundColor(selected == 3 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                        Circle()
                            .frame(width: 8, height: 8)
                            .foregroundColor(selected == 3 ? Color.orange : Color.clear)
                    }
                    .onTapGesture {
                        withAnimation(.spring()){
                            selected = 3
                        }
                    }
                    VStack(spacing: 10){
                        Text("Latte")
                            .foregroundColor(selected == 4 ? Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)) : Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
                        Circle()
                            .frame(width: 8, height: 8)
                            .foregroundColor(selected == 4 ? Color.orange : Color.clear)
                    }
                    .onTapGesture {
                        withAnimation(.spring()){
                            selected = 4
                        }
                    }
                }
                .font(.system(size: 16, weight: .medium))
                .padding(.horizontal)
            })
            .padding(.top, 32)
            .padding(.bottom, 16)
            
            ScrollView(.horizontal, showsIndicators: false, content: {
                HStack(spacing: 19){
                ForEach(0..<4){card in
                    NavigationLink(
                        destination: DetailView(),
                        label: {
                            ItemCardView()
                        })
                   
                }
                }
                .padding(.horizontal)
            })
            .padding(.bottom, 24)
            VStack(spacing: 18){
                Text("Special for you")
                    .font(.system(size: 18, weight: .semibold))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 1)))
                    .frame(maxWidth: .infinity, alignment: .leading)
                Image("t")
                    .resizable()
                    
            }
            .padding(.horizontal)
        }
        .padding(.bottom)
        }
        )
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.top)
        
        
    }
}

struct ItemCardView: View{
    var body: some View{
        VStack(spacing: 0){
            HStack(spacing: 5){
                Image("star")
                Text("4.2")
                    .foregroundColor(Color.init(#colorLiteral(red: 0.262745098, green: 0.1490196078, blue: 0.09411764706, alpha: 0.7)))
                    .font(.system(size: 12))
            }
            .padding(.vertical, 4)
            .padding(.horizontal, 9.5)
            .background(Color.init(#colorLiteral(red: 0.9803921569, green: 0.9529411765, blue: 0.9215686275, alpha: 1)))
            .cornerRadius(30)
            .padding(.top, 8)
            .padding(.trailing, 8)
            .frame(maxWidth: .infinity, alignment: .trailing)
            Image("cc")
                .frame(width: 58, height: 66)
                .padding(.vertical, 15)
            VStack(spacing: 0){
                Text("Cappuccino")
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.2588235294, green: 0.1490196078, blue: 0.09803921569, alpha: 1)))
                Text("with Oat milk")
                    .font(.system(size: 12, weight: .regular))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.1450980392, green: 0.08235294118, blue: 0.05490196078, alpha: 0.5)))
            }
            Spacer(minLength: 0)
            HStack{
                Text("$ 4.20")
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)))
                Spacer()
                Button(action: {
                    
                }, label: {
                    Image("bag")
                        .frame(width: 32, height: 32)
                        .background(Color.init(#colorLiteral(red: 0.5607843137, green: 0.3607843137, blue: 0.2705882353, alpha: 1)))
                        .clipShape(Circle())
                })
                    
            }
            .padding(.leading, 21)
            .padding(.trailing, 8)
            .padding(.bottom,8)
        }
        .frame(width: 162, height: 223)
        .background(Color.white)
        .cornerRadius(12)
        .overlay(RoundedRectangle(cornerRadius: 12).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.9294117647, green: 0.8352941176, blue: 0.7333333333, alpha: 1))))
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
